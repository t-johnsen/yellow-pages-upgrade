﻿using System;
using System.Collections.Generic;
using System.Text;

/*
 * Person class
 * Describes a person with attributes first name, last name and phone number.
 * Getters and setters
 * Print method and overloaded Contains method for searching
 */
namespace yellow_pages_upgrade
{
    class Person
    {

        private string firstname;
        private string lastname;
        private int phone;

        public string Firstname { get => firstname; set => firstname = value; }
        public string Lastname { get => lastname; set => lastname = value; }
        public int Phone { get => phone; set => phone = value; }

        public Person()
        {
        }
        public Person(string firstname, string lastname, int phone)
        {
            Firstname = firstname;
            Lastname = lastname;
            Phone = phone;

        }

        // Printing "this" object 
        public void Print()
        {
            Console.WriteLine($"Firstname: {this.firstname}. \nLastname: {this.lastname}. \nPhone: {this.phone}.");
        }

        
        // Overloading Contains() to search both partial and full searches. Not case sensitive.
        public bool Contains(string value)
        {
            return this.lastname.ToLower().Contains(value.ToLower()) || this.firstname.ToLower().Contains(value.ToLower());

        }

    }
}
