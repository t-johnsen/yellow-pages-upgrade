﻿using System;
using System.Collections.Generic;
using System.Linq;
using yellow_pages_upgrade;

namespace yellow_pages
{
    class Program
    {
        static void Main(string[] args)
        {

            List<Person> people = new List<Person>();
            people.Add(new Person("Mari", "Teiler-Johnsen", 98765432));
            people.Add(new Person("Erling", "Teiler-Johnsen", 90988432));
            people.Add(new Person("Aksel", "Andresen", 23332492));
            people.Add(new Person("Live", "Andresen", 72810593));
            people.Add(new Person("Rinsai", "Moontika", 27584938));
            people.Add(new Person());
            people[5].Firstname = "Hanne";
            people[5].Lastname = "Teiler-Johnsen";
            people[5].Phone = 95068888;

            // Application officially starts here where user can search and print the yellow pages
            // User can quit by typing 'q' in program or use 'print' to print list to console
            string searchString;
            do
            {
                Console.WriteLine("\nType something you want to search for below.");
                Console.WriteLine("To quit the program, type 'q'.");
                Console.WriteLine("To print entire yellow pages, type 'print'.");
                searchString = Console.ReadLine();
                if (searchString.Equals("print"))
                {
                    PrintAll(people);
                }
                else
                {
                    Search(people, searchString);
                }


            } while (!searchString.Equals("q"));

        }

        // Prints all people in the Person list 
        private static void PrintAll(List<Person> people)
        {
            if (!people.Any()) Console.WriteLine("No people in list.");
            foreach (Person person in people)
            {
                Console.WriteLine($"\nFirstname: {person.Firstname}. \nLastname: {person.Lastname}. \nPhone: {person.Phone}.");
            }
        }

        // Takes List<People> and string as params to search in people list.
        // Prints all results to console
        private static void Search(List<Person> people, string searchString)
        {
            Console.WriteLine($"Your search result containing '{searchString}':");
            PrintAll(people.FindAll(person => person.Contains(searchString)));

        }
    }
}
